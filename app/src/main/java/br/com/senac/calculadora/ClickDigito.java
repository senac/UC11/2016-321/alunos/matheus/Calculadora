package br.com.senac.calculadora;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sala304b on 15/08/2017.
 */

public class ClickDigito implements View.OnClickListener {

    private Display display;

    public ClickDigito(Display display) {
        this.display = display;
    }

    @Override
    public void onClick(View v) {
        String texto = display.getText().toString();
        Button botao = (Button) v;
        String digito = botao.getText().toString();

        if (display.isLimpaDisplay()) {
            display.setText(digito);
            display.setLimpaDisplay(false);
        } else {
            if (botao.getId() != R.id.btnPonto) {
                display.setText(texto + digito);
            } else {
                if (!texto.contains(".")) {
                    display.setText(texto + digito);
                }
            }
        }
    }


}






