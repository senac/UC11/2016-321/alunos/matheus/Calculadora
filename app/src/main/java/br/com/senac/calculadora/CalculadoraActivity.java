package br.com.senac.calculadora;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnDivisao;
    private Button btnMultiplicacao;
    private Button btnAdicao;
    private Button btnSubtracao;
    private Button btnPonto;
    private Button btnLimpar;
    private Button btnIgualdade;
    private TextView resultado;
    Display display;

    private static final char ADICAO = '+';
    private static final char SUBTRACAO = '-';
    private static final char DIVISAO = '/';
    private static final char MULTIPLICACAO = '*';

    private double operando1 = Double.NaN;
    private double operando2;
    private char operacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        this.btn0 = (Button) findViewById(R.id.btn0);
        this.btn1 = (Button) findViewById(R.id.btn1);
        this.btn2 = (Button) findViewById(R.id.btn2);
        this.btn3 = (Button) findViewById(R.id.btn3);
        this.btn4 = (Button) findViewById(R.id.btn4);
        this.btn5 = (Button) findViewById(R.id.btn5);
        this.btn6 = (Button) findViewById(R.id.btn6);
        this.btn7 = (Button) findViewById(R.id.btn7);
        this.btn8 = (Button) findViewById(R.id.btn8);
        this.btn9 = (Button) findViewById(R.id.btn9);
        this.btnPonto = (Button) findViewById(R.id.btnPonto);


        this.resultado = (TextView) findViewById(R.id.resultado);

        display = new Display(this.resultado);

        ClickDigito clickDigito = new ClickDigito(display);

        this.btn0.setOnClickListener(clickDigito);
        this.btn1.setOnClickListener(clickDigito);
        this.btn2.setOnClickListener(clickDigito);
        this.btn3.setOnClickListener(clickDigito);
        this.btn4.setOnClickListener(clickDigito);
        this.btn5.setOnClickListener(clickDigito);
        this.btn6.setOnClickListener(clickDigito);
        this.btn7.setOnClickListener(clickDigito);
        this.btn8.setOnClickListener(clickDigito);
        this.btn9.setOnClickListener(clickDigito);

        this.btnPonto.setOnClickListener(clickDigito);


        this.btnLimpar = (Button) findViewById(R.id.btnLimpar);

        this.btnLimpar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        limparDisplay(true);
                    }
                }
        );

        this.btnAdicao = (Button) findViewById(R.id.btnAdicao);
        this.btnSubtracao = (Button) findViewById(R.id.btnSubtracao);
        this.btnDivisao = (Button) findViewById(R.id.btnDivisao);
        this.btnMultiplicacao = (Button) findViewById(R.id.btnMultiplicacao);
        this.btnIgualdade = (Button) findViewById(R.id.btnIgualdade);

        this.btnAdicao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = ADICAO;
                calculado = false;
                calcular();

            }
        });

        this.btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = SUBTRACAO;
                calculado = false;
                calcular();
            }
        });

        this.btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = DIVISAO;
                calculado = false;
                calcular();
            }
        });

        this.btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = MULTIPLICACAO;
                calculado = false;
                calcular();
            }
        });

        this.btnIgualdade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setLimpaDisplay(false);
                calculado = true;
                calcular();
            }
        });


    }

    private boolean calculado = false;

    private void calcular() {

        if (Double.isNaN(operando1)) {

            operando1 = display.getValue();

            limparDisplay(false);

        } else {
            if (!display.isLimpaDisplay()) {
                if (!calculado) {

                    operando2 = display.getValue();
                }

                switch (operacao) {

                    case ADICAO:
                        operando1 = operando1 + operando2;
                        break;
                    case SUBTRACAO:
                        operando1 = operando1 - operando2;
                        break;
                    case DIVISAO:

                        if (operando2 != 0) {

                            operando1 = operando1 / operando2;
                        } else {
                            Context context = getApplicationContext();
                            String mensagem = "Impossível dividir por zero!";
                            int duracao = Toast.LENGTH_LONG;

                            Toast toast = Toast.makeText(context, mensagem, duracao);

                            toast.show();
                        }
                        operando1 = operando1 / operando2;
                        break;
                    case MULTIPLICACAO:
                        operando1 = operando1 * operando2;
                        break;
                }

                limparDisplay(false);
                display.setText(operando1);
            }
        }
    }


    private void limparDisplay(boolean zerar) {

        if (zerar) {
            display.setText("0");
            operando1 = Double.NaN;
            calculado = false;
        }

        display.setLimpaDisplay(true);


    }


}

